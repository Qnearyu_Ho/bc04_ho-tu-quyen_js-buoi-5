/*
    giaTien50KwDauTien = 500;
    giaTien50KwKe = 650;
    giaTien100KwKe = 850 ;
    giaTien150KwKe = 1100;
    giaTienKwConLai = 1300;

    INPUT:
    Nhập vào họ tên
    Nhập vào số kw
    TODO:
    Sử dụng câu lệnh if..else cho các trường hợp
    - Trường hợp 50 kw đầu tiên: tổng tiền điện = số kw * 500;
    - Trường hợp cho 50 kw kế tiếp: tổng tiền điện =  50*500 + (số kw - 50)*650;
    - Trường hợp cho 100 kw kế tiếp: tổng tiền điện = 50*500 + 50*650 + (sokw - 100)*850 ;
    - Trường hợp cho 150 kw kế tiếp: tổng tiền điện = 50*500 + 50*650 + 100*850 + (sokw - 200)*1100;
    - Trường hợp cho số kw còn lại: tổng tiền điện = 50*500 + 50*650 + 100*850 + 150*1100 + (sokw - 350)*1300;
    OUTPUT:
    In ra tổng tiền điện cho số kw được nhập vào



*/ 
function tinhTien(){
    var hoTen = document.getElementById("name").value;
    var sokw = document.getElementById("sokw").value;

    var result = 0;
    if (sokw <= 50){
        result = sokw * 500;
    } 
    else if(sokw <= 100){
        result = 50*500 + (sokw - 50)*650;
    }
    else if(sokw <= 200){
        result = 50*500 + 50*650 + (sokw - 100)*850;
    }
    else if(sokw <= 350){
        result = 50*500 + 50*650 + 100*850 + (sokw - 200)*1100;
    }
    else{
        result = 50*500 + 50*650 + 100*850 + 150*1100 + (sokw - 350)*1300;

    }
    document.getElementById("result").innerHTML = `<p> Họ tên: ${hoTen}, Tổng tiền điện: ${result} d`;

}