/*
INPUT: Nhập vào điểm chuẩn của trường, điểm ưu tiên theo khu vực và đối tượng, điểm của 3 môn
TODO: tổng điểm bằng 3 môn thi + điểm ưu tiên theo khu vực + điểm ưu tiên theo đối tượng
So sánh
Nếu điểm tổng < điểm chuẩn của trường => không đậu
Còn khi điểm tổng > điểm chuẩn của trường => đậu
*/ 
function tinh(){
    var diemChuan = document.getElementById("diemChuan").value*1;
    var diemKhuVuc = document.getElementById("diemKhuVuc");
    var diemTheoKhuVuc = diemKhuVuc.options[diemKhuVuc.selectedIndex].value*1;
    var diemDoiTuong = document.getElementById("diemDoiTuong");
    var diemTheoDoiTuong = diemDoiTuong.options[diemDoiTuong.selectedIndex].value*1;
    var diemMon1 = document.getElementById("diem1").value*1;
    var diemMon2 = document.getElementById("diem2").value*1;
    var diemMon3 = document.getElementById("diem3").value*1;
    var diemTong;
    var ketQua;

    diemTong = diemMon1 + diemMon2 + diemMon3 + diemTheoKhuVuc + diemTheoDoiTuong;
    if(diemTong < diemChuan){
      ketQua = "Bạn không đậu vào trường này"  
    } else {
        ketQua = "Bạn đã đậu vào trường này"
    }
    document.getElementById("result").innerHTML = `<p> ${ketQua} với tổng điểm: ${diemTong} `;

}